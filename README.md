# Kicking It With PyDICOM

A repository of some helpful DICOM scripts that I have started to maintain.

dcm_anonymise
=============

Usage: dcm_anonymise.py [options]

Options: -h, --help show this help message and exit -p PN,
--patient\_name=PN Patient name override. -s SID, --series\_id=SID
Series ID to override. -d SD, --description=SD Study description
override. -r TID, --reference\_id=TID A reference id from redmine or
trac etc. For e.g: rm\#3511 -i DCMDIR, --directory=DCMDIR Directory
containing dicom files. -D DEST, --destination=DEST Optional destination
directory. If unspecified, patient name is used for the directory name.
-l, --list\_tags List of Tags that will be anonymised. -v, --version
Print current version. -f, --force Skip some checks such as dcm
extension.


