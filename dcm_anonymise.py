#!python

import os
import sys
import dicom
from optparse import OptionParser

version = "0.9"
anon_folder = "Anonymised"

def main():
    if True:
        parser = OptionParser()
        parser.add_option("-p", "--patient_name", dest="m_patient_name", default=None, help="Patient name override.", metavar="PN")
        parser.add_option("-S", "--study_id", dest="m_study_id", default=None, help="Study ID to override.", metavar="SSID")
        parser.add_option("-s", "--series_id", dest="m_series_id", default=None, help="Series ID to override.", metavar="SID")
        parser.add_option("-d", "--description", dest="m_description", default=None, help="Study description override.", metavar="SD")
        parser.add_option("-r", "--reference_id", dest="m_reference_id", default=None, help="A reference id from redmine or trac etc. For e.g: rm#3511", metavar="TID")
        parser.add_option("-i", "--directory", dest="m_directory", default=None, help="Directory containing dicom files.", metavar="DCMDIR")
        parser.add_option("-D", "--destination", dest="m_destination", default=None, help="Optional destination directory. If unspecified, patient name is used for the directory name.", metavar="DEST")
        parser.add_option("-m", "--modality", dest="m_modality", default=None, help="Optional modality override.", metavar="MODA")
        parser.add_option("-l", "--list_tags", dest="m_list_tags", action="store_true", help="List of Tags that will be anonymised.", default=False, metavar="LOT")
        parser.add_option("-v", "--version", dest="m_version", action="store_true", help="Print current version.", default=False)
        parser.add_option("-f", "--force", dest="m_force", action="store_true", help="Skip some checks such as dcm extension.", default=False)

        (options, args) = parser.parse_args()

        if ( options.m_version ):
            print "VERSION: " + version
            return 0

        tags = [
                0x00080080,#InstitutionName
                0x00080081,#InstitutionAddress
                0x00080090,#ReferringPhysicianName
                0x00080092,#ReferringPhysicianAddress
                0x00080094,#ReferringPhysicianTelephoneNumbers
                0x00081010,#StationName
                0x00081040,#InstitutionDepartmentName
                0x00081048,#PhysiciansOfRecord
                0x00081050,#PerformingPhysicianName
                0x00081060,#NameOfPhysiciansReadingStudy
                0x00081070,#OperatorsName
                0x00081080,#AdmittingDiagnosesDescription
                0x00082111,#DerivationDescription
                0x00100030,#PatientBirthDate
                0x00100032,#PatientBirthTime
                0x00101000,#OtherPatientIds
                0x00101001,#OtherPatientNames
                0x00101005,#PatientsBirthName
                0x00101010,#PatientAge
                0x00101020,#PatientSize
                0x00101040,#PatientsAddress
                0x00101060,#PatientsMotherBirthName
                0x00101090,#MedicalRecorLocator
                0x00102180,#Occupation
                0x001021b0,#AdditionalPatientHistory
                0x00104000,#PatientsComments
                0x00181030,#ProtocolName
                0x00204000,#ImageComments
                0x00321032,#RequestingPhysiciansName
        ]

        if ( options.m_list_tags ):
            print ( "LIST OF SUPPORTED %d TAGS:"%len(tags) )
            print ( "\tTAG\t\tDESCRIPTION" )

            for tag in tags:
                fs = "\t" + hex(tag) + " \t" + dicom.datadict.dictionary_description( tag ) 
                print fs

            sys.stdout.flush()
            return 0


        if ( options.m_directory is None ):
            print "ERROR: Input directory not specified."
            return -1

        if ( not os.path.exists( options.m_directory ) ):
            print "ERROR: Specified path does not exist."
            return -1

        description = None
        if ( not options.m_description is None ):
            description = str(options.m_description) + " "

        if ( not options.m_reference_id is None ):
            if (description is None):
                description = "Reference ID: " + str(options.m_reference_id)
            else:
                description = description + "Reference ID: " + str(options.m_reference_id)

        print "Input Directory:\t\t" + options.m_directory
        print "Description:\t\t" + description
        if ( not options.m_destination is None ):
            anon_folder = options.m_destination + "/"
        elif ( not options.m_patient_name is None ):
            anon_folder = options.m_patient_name + "/"
        print "Destination Directory:\t\t" + anon_folder

        aid = 0 
        for root, dirs, files in os.walk( options.m_directory ):

            if (len(files) == 0):
                continue

            if ( not options.m_force and not files[0].endswith( '.dcm' ) ):
                continue 

            aid = aid + 1

            dst_path = anon_folder
            if ( not os.path.exists( dst_path ) ):
                os.mkdir(dst_path)

            print root, " -> ", dst_path
            sys.stdout.flush()

            fid = -1
            for fp in files:
                fid = fid + 1

                ds = dicom.read_file( root + "/" + fp )

                for tag in tags:
                    if ds.has_key(tag):
                        del ds[tag]

                if ( not options.m_series_id is None ):
                    series_id = str(options.m_series_id)
                else:
                    series_id = str(aid)

                ds[0x00200011].value = series_id

                if ( not options.m_study_id is None ):
                    ds.StudyID = options.m_study_id

                if ( not options.m_patient_name is None ):
                    if ( ds.has_key( 0x00100010 ) ): ds[0x00100010].value = options.m_patient_name

                if ( not description is None ):
                    ds.StudyDescription = description
                    ds.SeriesDescription = description

                if ( not options.m_modality is None ):
                    ds.Modality = options.m_modality

                ds.save_as( dst_path + "%s_"%series_id.zfill(2) + str(fid).zfill(3) + ".dcm" )


    raw_input("Press any key to exit.")
    return 0

if __name__ == '__main__':
    main()
    pass
